require 'will_paginate/view_helpers/action_view'

module Pagination
  class BootstrapRenderer < ::WillPaginate::ActionView::LinkRenderer

    protected

    def html_container(html)
      tag :nav, tag(:ul, html, class: 'pagination'), container_attributes.merge(class: 'pagination justify-content-center')
    end

    def page_number(page)
      tag :li, link(page, page, rel: rel_value(page), class: 'page-link'), class: page == current_page ? 'page-item active' : 'page-item'
    end

    def gap
      tag :li, link(super, '#', class: 'page-link'), class: 'page-item disabled'
    end

    def previous_or_next_page(page, text, classname)
      tag :li, link(text, page || '#', class: 'page-link'), class: [classname[0..3], classname, ('disabled' unless page)].join(' ')
    end
  end
end
