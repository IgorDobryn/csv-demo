desc "Populate test data."
task add_test_data: ['db:drop', 'db:create', 'db:migrate', 'db:seed', 'environment'] do
  raise 'Unsupported environment' unless Rails.env.development?

  FactoryBot.create_list(:supplier, 10).each do |supplier|
    FactoryBot.create_list :sku, rand(4) + 1, supplier: supplier
  end
end
