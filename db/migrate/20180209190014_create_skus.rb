class CreateSkus < ActiveRecord::Migration[5.1]
  def change
    create_table :skus do |t|
      t.belongs_to :supplier, foreign_key: true, index: true, null: false
      t.string :field_1, null: false
      t.string :field_2, null: false
      t.string :field_3, null: false
      t.string :field_4, null: false
      t.string :field_5, null: false
      t.string :field_6, null: false

      t.timestamps
    end
  end
end
