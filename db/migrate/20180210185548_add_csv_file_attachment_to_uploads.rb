class AddCsvFileAttachmentToUploads < ActiveRecord::Migration[5.1]
  def change
    add_attachment :uploads, :csv_file
  end
end
