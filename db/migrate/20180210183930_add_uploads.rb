class AddUploads < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          CREATE TYPE upload_status AS ENUM ('pending', 'in_progress', 'finished');
        SQL
      end

      dir.down do
        execute <<-SQL
          DROP TYPE upload_status;
        SQL
      end
    end

    create_table :uploads do |t|
      t.integer :rows_count, null: false, default: 0
      t.integer :processed_rows_count, null: false, default: 0
      t.column :status, :upload_status, null: false

      t.timestamps null: false
    end
  end
end
