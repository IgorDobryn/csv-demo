class AddConstraintsForUploadCounts < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          ALTER TABLE uploads ADD CONSTRAINT rows_count_check CHECK (
           rows_count >= 0
          );

          ALTER TABLE uploads ADD CONSTRAINT processed_rows_count_check CHECK (
           processed_rows_count BETWEEN 0 AND rows_count
          );
        SQL
      end

      dir.down do
        execute <<-SQL
          ALTER TABLE uploads DROP CONSTRAINT rows_count_check;
          ALTER TABLE uploads DROP CONSTRAINT processed_rows_count_check;
        SQL
      end
    end
  end
end
