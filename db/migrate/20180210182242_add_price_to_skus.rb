class AddPriceToSkus < ActiveRecord::Migration[5.1]
  def change
    add_column :skus, :price, :decimal, precision: 8, scale: 2, null: false

    reversible do |dir|
      dir.up do
        execute <<-SQL
          ALTER TABLE skus ADD CONSTRAINT price_check CHECK (
           price > 0
          );
        SQL
      end

      dir.down do
        execute <<-SQL
          ALTER TABLE skus DROP CONSTRAINT price_check;
        SQL
      end
    end
  end
end
