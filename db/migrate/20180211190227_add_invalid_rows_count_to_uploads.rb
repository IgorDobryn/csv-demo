class AddInvalidRowsCountToUploads < ActiveRecord::Migration[5.1]
  def change
    add_column :uploads, :invalid_rows_count, :integer, null: false, default: 0

    reversible do |dir|
      dir.up do
        execute <<-SQL
          ALTER TABLE uploads ADD CONSTRAINT invalid_rows_count_check CHECK (
           processed_rows_count BETWEEN 0 AND rows_count
          );
        SQL
      end

      dir.down do
        execute <<-SQL
          ALTER TABLE uploads DROP CONSTRAINT processed_rows_count_check;
        SQL
      end
    end
  end
end
