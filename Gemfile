source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


gem 'rails', '~> 5.1.4'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'webpacker'

gem 'haml'
gem 'will_paginate'
gem 'aasm'
gem 'paperclip'
gem 'smarter_csv'
gem 'sidekiq'
gem 'parallel'

group :development, :test do
  gem 'capybara', '~> 2.13'
  gem 'database_cleaner'
  gem 'capybara-webkit'
  gem 'capybara-screenshot'

  gem 'rspec-rails'
  gem 'rspec-instafail', require: false
  gem 'factory_bot_rails'
  gem 'faker'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers'
end
