require 'rails_helper'

RSpec.describe SkusCsvParser, type: :feature do
  describe '#headers' do
    subject { described_class.headers }

    it { is_expected.to eql([:id, :supplier_id, :field_1, :field_2, :field_3, :field_4, :field_5, :field_6, :price]) }
    it { expect(described_class.model.attribute_names.map(&:to_sym)).to include(*subject) }
  end

  describe '#model' do
    subject { described_class.model }

    it { is_expected.to eql(Sku) }
  end

  describe '#parse!' do
    subject { described_class.new(upload).parse! }

    let(:supplier1) { create :supplier, id: 123 }
    let(:supplier2) { create :supplier, id: 124 }

    let!(:sku1) { create :sku, supplier: supplier1, id: 234, field_1: 'val 10', field_2: 'val 20',
                         field_3: 'val 30', field_4: 'val 40', field_5: 'val 50', field_6: 'val 60',
                         price: 123.45 }
    let!(:sku2) { create :sku, supplier: supplier2, id: 235, field_1: 'val 100', field_2: 'val 200',
                         field_3: 'val 300', field_4: 'val 400', field_5: 'val 500', field_6: 'val 600',
                         price: 234.56 }

    let(:csv_file) { create :skus_csv_file, rows: [
        [234, 's124', 'val 11', 'val 20', 'val 31', 'val 40', 'val 51', 'val 60', 123.45],
        [235, 's124', 'val 100', 'val 200', 'val 300', 'val 400', 'val 500', 'val 600', 234.56],
        [236, 's123', 'val 300', 'val 400', 'val 500', 'val 600', 'val 700', 'val 800', 100.10],
    ] }

    let(:upload)  { create :upload, csv_file: csv_file.open }

    it 'performs multiple changes to suppliers' do
      expect(Sku.exists?(236)).to be_falsey

      expect { subject }.to change { Sku.count }.by(1)

      expect(Sku.find(236)).to have_attributes({ supplier_id: 123, field_1: 'val 300',
                                                 field_2: 'val 400', field_3: 'val 500',
                                                 field_4: 'val 600', field_5: 'val 700',
                                                 field_6: 'val 800', price: 100.10 })

      expect(sku1.reload).to have_attributes({ supplier_id: 124, field_1: 'val 11',
                                               field_2: 'val 20', field_3: 'val 31',
                                               field_4: 'val 40', field_5: 'val 51',
                                               field_6: 'val 60', price: 123.45 })

      expect(sku2.reload).to have_attributes({ supplier_id: 124, field_1: 'val 100',
                                               field_2: 'val 200', field_3: 'val 300',
                                               field_4: 'val 400', field_5: 'val 500',
                                               field_6: 'val 600', price: 234.56 })
    end
  end
end
