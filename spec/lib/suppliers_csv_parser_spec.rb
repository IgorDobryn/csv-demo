require 'rails_helper'

RSpec.describe SuppliersCsvParser, type: :feature do
  describe '#headers' do
    subject { described_class.headers }

    it { is_expected.to eql([:id, :name]) }
    it { expect(described_class.model.attribute_names.map(&:to_sym)).to include(*subject) }
  end

  describe '#model' do
    subject { described_class.model }

    it { is_expected.to eql(Supplier) }
  end

  describe '#parse!' do
    subject { described_class.new(upload).parse! }

    let!(:supplier1) { create :supplier, id: 123, name: 'Will be Changed' }
    let!(:supplier2) { create :supplier, id: 124, name: 'Stays Unchanged' }
    let!(:supplier3) { create :supplier, id: 125, name: 'Will be Changed Multiple times' }

    let(:csv_file) { create :suppliers_csv_file, rows: [
        ['s123', 'Is Changed'],
        ['s124', ' Stays Unchanged '],
        ['s125', 'Changed 1st Time'],
        ['s125', 'Changed 2nd Time'],
        ['s126', 'New Supplier'],
        ['s127', ''],
    ] }

    let(:upload)  { create :upload, csv_file: csv_file.open }

    it 'performs multiple changes to suppliers' do
      expect(Supplier.exists?(126)).to be_falsey
      expect(upload.rows_count).to eql(0)
      expect(upload.processed_rows_count).to eql(0)
      expect(upload.invalid_rows_count).to eql(0)

      expect { subject }.to change { Supplier.count }.by(1)

      expect(upload.rows_count).to eql(6)
      expect(upload.processed_rows_count).to eql(5)
      expect(upload.invalid_rows_count).to eql(1)
      expect(Supplier.find(126)).to have_attributes({ name: 'New Supplier' })
      expect(supplier1.reload).to have_attributes({ name: 'Is Changed' })
      expect(supplier3.reload).to have_attributes({ name: 'Changed 2nd Time' })
      expect(supplier2.reload).to have_attributes({ name: 'Stays Unchanged' })
    end
  end
end
