require 'rails_helper'

RSpec.describe SupplierIdConverter do
  describe '#convert' do

    it { expect(described_class.convert("s12345")).to eql(12345) }
    it { expect(described_class.convert(12345)).to eql(12345) }
    it { expect(described_class.convert("ssss")).to be_nil }
  end
end
