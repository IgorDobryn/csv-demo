require 'rails_helper'

RSpec.describe UploadCsvParser, type: :feature do
  let(:upload) { create :upload }

  describe '#headers' do
    it { expect { described_class.headers }.to raise_error(NotImplementedError) }
  end

  describe '#parse!' do
    let(:upload) { create :upload, csv_file: create(:suppliers_csv_file, rows_count: 7).open }
    subject { described_class.for(upload) }

    it { expect { subject.parse! }.to change { upload.status }.from('pending').to('finished') }
    it { expect { subject.parse! }.to change { upload.rows_count }.from(0).to(7) }
    it { expect { subject.parse! }.to change { upload.processed_rows_count }.from(0).to(7) }
  end

  describe '#for' do
    subject { described_class.for(upload) }

    context 'when skus upload' do
      let(:upload) { create :upload, csv_file: create(:skus_csv_file, rows_count: 7).open }

      it { is_expected.to be_kind_of(SkusCsvParser) }
    end

    context 'when suppliers upload' do
      let(:upload) { create :upload, csv_file: create(:suppliers_csv_file, rows_count: 7).open }

      it { is_expected.to be_kind_of(SuppliersCsvParser) }
    end
  end
end
