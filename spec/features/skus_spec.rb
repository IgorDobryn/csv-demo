require 'rails_helper'

describe 'Skus Index Page' do
  it 'renders skus index' do
    supplier = create :supplier, name: 'My Supplier'
    create :sku, supplier: supplier, id: 1_234, field_1: 'value 1', field_2: 'value 2', field_3: 'value 3',
           field_4: 'value 4', field_5: 'value 5', field_6: 'value 6', price: '321.12'

    visit root_path

    within 'table.skus' do
      expect(page.all('thead th').map(&:text)).to eql(["Id", "Supplier", "Field 1", "Field 2", "Field 3",
                                                       "Field 4", "Field 5", "Field 6", "Price"])

      within '#sku_1234' do
        expect(page).to have_selector('.id', text: '1234')
        expect(page).to have_selector('.supplier', text: 'My Supplier')
        expect(page).to have_selector('.field_1', text: 'value 1')
        expect(page).to have_selector('.field_2', text: 'value 2')
        expect(page).to have_selector('.field_3', text: 'value 3')
        expect(page).to have_selector('.field_4', text: 'value 4')
        expect(page).to have_selector('.field_5', text: 'value 5')
        expect(page).to have_selector('.field_6', text: 'value 6')
        expect(page).to have_selector('.price', text: '$321.12')
      end
    end
  end
end
