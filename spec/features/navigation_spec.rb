require 'rails_helper'

describe 'Navigation' do
  it 'navigates through top navigation' do
    visit root_path

    within '.navbar' do
      expect(page).to have_link('Uploads', href: uploads_path)
      expect(page).to have_link('Skus', href: skus_path)
      expect(page).to have_selector('a', count: 2)
      expect(page).to have_selector('.nav-item.active', text: 'Skus')

      click_link 'Uploads'
    end

    within '.navbar' do
      expect(page).to have_link('Uploads', href: uploads_path)
      expect(page).to have_link('Skus', href: skus_path)
      expect(page).to have_selector('a', count: 2)
      expect(page).to have_selector('.nav-item.active', text: 'Uploads')

      click_link 'Skus'
    end

    within '.navbar' do
      expect(page).to have_link('Uploads', href: uploads_path)
      expect(page).to have_link('Skus', href: skus_path)
      expect(page).to have_selector('a', count: 2)
      expect(page).to have_selector('.nav-item.active', text: 'Skus')
    end
  end
end
