require 'rails_helper'

describe 'Uploads', js: true do
  include ActiveJob::TestHelper

  it 'allows to create upload with csv attached' do
    suppliers_csv = create :suppliers_csv_file, rows: [
        ['s123', 'Sup 1'],
        ['s124', 'Sup 2'],
    ]
    skus_csv = create :skus_csv_file, rows: [
        [234, 's124', 'val 11', 'val 20', 'val 31', 'val 40', 'val 51', 'val 60', 123.45],
        [235, 's123', 'val 300', 'val 400', 'val 500', 'val 600', 'val 700', 'val 800', 100.10],
    ]

    visit root_path

    expect(page).to have_no_selector('table.skus tbody tr')

    perform_enqueued_jobs do
      click_link 'Upload CSV'

      attach_file 'CSV File', suppliers_csv.filename
      click_button 'Submit'

      expect(page).to have_selector('#flash-messages', text: 'Successfully created new upload')
    end
    expect(Supplier.count).to eql(2)

    perform_enqueued_jobs do
      click_link 'Upload CSV'

      attach_file 'CSV File', skus_csv.filename
      click_button 'Submit'

      expect(page).to have_selector('#flash-messages', text: 'Successfully created new upload')
    end

    within 'table.skus' do
      expect(page).to have_selector('tbody tr', count: 2)

      within '#sku_234' do
        expect(page).to have_selector('.id', text: '234')
        expect(page).to have_selector('.supplier', text: 'Sup 2')
        expect(page).to have_selector('.field_1', text: 'val 11')
        expect(page).to have_selector('.field_2', text: 'val 20')
        expect(page).to have_selector('.field_3', text: 'val 31')
        expect(page).to have_selector('.field_4', text: 'val 40')
        expect(page).to have_selector('.field_5', text: 'val 51')
        expect(page).to have_selector('.field_6', text: 'val 60')
        expect(page).to have_selector('.price', text: '$123.45')
      end

      within '#sku_235' do
        expect(page).to have_selector('.id', text: '235')
        expect(page).to have_selector('.supplier', text: 'Sup 1')
        expect(page).to have_selector('.field_1', text: 'val 300')
        expect(page).to have_selector('.field_2', text: 'val 400')
        expect(page).to have_selector('.field_3', text: 'val 500')
        expect(page).to have_selector('.field_4', text: 'val 600')
        expect(page).to have_selector('.field_5', text: 'val 700')
        expect(page).to have_selector('.field_6', text: 'val 800')
        expect(page).to have_selector('.price', text: '$100.10')
      end
    end
  end

  it 'renders uploads index' do
    create :upload, id: 123, csv_file: create(:suppliers_csv_file, name: 'suppliers #1.csv').open,
           processed_rows_count: 33, rows_count: 77
    create :upload, :in_progress, id: 234, csv_file: create(:suppliers_csv_file, name: 'suppliers #2.csv').open,
           processed_rows_count: 33, rows_count: 77
    create :upload, :finished, id: 345, csv_file: create(:suppliers_csv_file, name: 'suppliers #3.csv').open,
           processed_rows_count: 77, rows_count: 77
    create :upload, :finished, id: 346, csv_file: create(:suppliers_csv_file, name: 'suppliers #4.csv').open,
           processed_rows_count: 64, rows_count: 77, invalid_rows_count: 13

    visit uploads_path

    within 'table.uploads' do
      expect(page.all('thead th').map(&:text)).to eql(["Id", "Status", "Csv file name"])

      within '#upload_123' do
        expect(page).to have_selector('.id', text: '123')
        expect(page).to have_selector('.status', text: 'Pending')
        expect(page).to have_selector('.csv-file-name', text: 'suppliers__1.csv')
      end

      within '#upload_234' do
        expect(page).to have_selector('.id', text: '234')
        expect(page).to have_selector('.status', text: 'Processed 33 of 77 records')
        expect(page).to have_selector('.csv-file-name', text: 'suppliers__2.csv')
      end

      within '#upload_345' do
        expect(page).to have_selector('.id', text: '345')
        expect(page).to have_selector('.status', text: 'Finished processing of 77 records')
        expect(page).to have_selector('.csv-file-name', text: 'suppliers__3.csv')
      end

      within '#upload_346' do
        expect(page).to have_selector('.id', text: '346')
        expect(page).to have_selector('.status', text: 'Finished processing of 77 records')
        expect(page).to have_selector('.status', text: '13 invalid row(s) found')
        expect(page).to have_selector('.csv-file-name', text: 'suppliers__4.csv')
      end
    end
  end
end
