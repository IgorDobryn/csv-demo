require 'rails_helper'

RSpec.describe Sku, type: :model do
  it { is_expected.to validate_presence_of(:supplier).with_message('must exist') }
  it { is_expected.to validate_presence_of(:field_1) }
  it { is_expected.to validate_presence_of(:field_2) }
  it { is_expected.to validate_presence_of(:field_3) }
  it { is_expected.to validate_presence_of(:field_4) }
  it { is_expected.to validate_presence_of(:field_5) }
  it { is_expected.to validate_presence_of(:field_6) }
  it { is_expected.to validate_presence_of(:price) }
  it { is_expected.to validate_numericality_of(:price).is_greater_than(0) }
end
