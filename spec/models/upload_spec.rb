require 'rails_helper'

RSpec.describe Upload, type: :model do
  include ActiveJob::TestHelper

  it { expect(described_class.new.status).to eql('pending') }
  it { is_expected.to validate_numericality_of(:rows_count) }
  it { is_expected.to validate_numericality_of(:processed_rows_count) }
  it { is_expected.to_not allow_value(nil).for(:processed_rows_count) }
  it { is_expected.to_not allow_value(nil).for(:rows_count) }
  it { expect(build(:upload, rows_count: 10)).to allow_value(10).for(:processed_rows_count) }
  it { expect(build(:upload, rows_count: 10)).to_not allow_value(11).for(:processed_rows_count) }

  it { is_expected.to_not allow_value(nil).for(:csv_file) }
  it { is_expected.to_not allow_value(Tempfile.new('suppliers')).for(:csv_file)
                              .with_message('is invalid')
                              .with_message('is invalid', against: :csv_file_content_type)
                              .with_message('is not detected. Check csv file name and upload once more', against: :upload_type) }
  it { is_expected.to_not allow_value(Tempfile.new('suppliers.csv')).for(:csv_file)
                              .with_message('has contents that are not what they are reported to be')
                              .with_message('is invalid')
                              .with_message('is invalid', against: :csv_file_content_type) }

  it { is_expected.to allow_value(create(:suppliers_csv_file, name: 'suppliers').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:suppliers_csv_file, name: 'supplier').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:suppliers_csv_file, name: 'SUPPLIERS').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:suppliers_csv_file, name: 'suppliers-2018-02-11').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:skus_csv_file, name: 'skus').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:skus_csv_file, name: 'sku').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:skus_csv_file, name: 'SKUS').open).for(:csv_file) }
  it { is_expected.to allow_value(create(:skus_csv_file, name: 'skus-2018-02-11').open).for(:csv_file) }

  describe '#run!' do
    subject { create(:upload) }

    it { expect { subject.run! }.to change { subject.status }.from('pending').to('in_progress') }
  end

  describe '#finish!' do
    subject { create(:upload, :in_progress) }

    it { expect { subject.finish! }.to change { subject.status }.from('in_progress').to('finished') }

    context 'when processed rows count differs from rows count' do
      subject { create(:upload, :in_progress, processed_rows_count: 10, rows_count: 10).tap { |u| u.processed_rows_count = 11 } }

      it { expect { subject.finish! }.to raise_error(AASM::InvalidTransition) }
    end
  end

  describe '#schedule_parse_csv_job' do
    let(:upload) { create(:upload)}

    it "schedules ParseCsvJob with specific upload instance" do
      assert_enqueued_with job: ParseCsvJob, args: [upload] do
        upload.schedule_parse_csv_job
      end
    end
  end

  describe '#parse_csv!' do
    let(:upload) { create(:upload)}

    it { expect { upload.parse_csv! }.to change { upload.status }.from('pending').to('finished') }
  end

  describe '#upload_type' do
    subject { upload.upload_type }

    context 'when suppliers csv' do
      let(:upload) { create(:upload, csv_file: create(:suppliers_csv_file).open) }

      it { is_expected.to eql(:supplier) }
    end

    context 'when skus csv' do
      let(:upload) { create(:upload, csv_file: create(:skus_csv_file).open) }

      it { is_expected.to eql(:sku) }
    end

    context 'when invalid named csv' do
      let(:upload) { build(:upload, csv_file: create(:skus_csv_file, name: 'invalid').open) }

      it { is_expected.to be_nil }
    end
  end
end
