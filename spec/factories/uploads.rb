FactoryBot.define do
  factory :upload do
    csv_file { create(:suppliers_csv_file).open }

    trait :in_progress do
      after :create do |upload|
        upload.run!
      end
    end

    trait :finished do
      after :create do |upload|
        upload.run!
        upload.finish!
      end
    end
  end
end
