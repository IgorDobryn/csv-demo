require 'csv'

class CsvFileBuilder
  attr_accessor :name
  attr_accessor :columns
  attr_accessor :rows
  attr_accessor :col_sep

  def filename
    Rails.root.join(dirname, name)
  end

  def dirname
    Rails.root.join('tmp', "test#{ENV['TEST_ENV_NUMBER']}")
  end

  def open
    File.open(filename, 'rt')
  end

  def save!
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)

    CSV.open(filename, 'wt+', col_sep: col_sep) do |file|
      file << columns if columns.present?

      rows.each { |r| file << r }
    end
  end
end

FactoryBot.define do
  sequence :supplier_csv_row do |n|
    ["#{'s%.4i' % n}", "#{Faker::Company.name} #{n}"]
  end

  sequence :skus_csv_row do |n|
    ["#{'s%.8i' % n}", "#{'s%.4i' % Faker::Number.number(4)}"] +
        Array.new(6) { "value #{Faker::Number.number(4)}" } +
        [Faker::Commerce.price(1...10_000)]
  end

  factory :csv_file, class: CsvFileBuilder do
    name "#{Faker::Number.number(6)}-#{Time.current.to_i}.csv"
    columns []
    rows []
    col_sep UploadCsvParser::COL_SEP

    trait :suppliers do
      transient do
        rows_count 1
      end

      name "suppliers-#{Faker::Number.number(6)}-#{Time.current.to_i}.csv"
      rows { Array.new(rows_count) { generate(:supplier_csv_row) } }
    end

    trait :skus do
      transient do
        rows_count 1
      end

      name "skus-#{Faker::Number.number(6)}-#{Time.current.to_i}.csv"
      rows { Array.new(rows_count) { generate(:skus_csv_row) } }
    end

    factory :suppliers_csv_file, traits: [:suppliers]
    factory :skus_csv_file, traits: [:skus]
  end
end
