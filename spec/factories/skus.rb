FactoryBot.define do
  sequence(:field) { |n| "#{Faker::Lorem.word} #{n}" }

  factory :sku do
    association :supplier

    sequence(:field_1) { generate(:field) }
    sequence(:field_2) { generate(:field) }
    sequence(:field_3) { generate(:field) }
    sequence(:field_4) { generate(:field) }
    sequence(:field_5) { generate(:field) }
    sequence(:field_6) { generate(:field) }
    price { Faker::Commerce.price }
  end
end
