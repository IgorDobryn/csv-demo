require 'rails_helper'

RSpec.describe ParseCsvJob, type: :job do
  let(:upload) { build :upload }

  describe '#perform' do
    it 'publishes publication' do
      expect(upload).to receive(:parse_csv!)

      described_class.perform_now upload
    end
  end
end
