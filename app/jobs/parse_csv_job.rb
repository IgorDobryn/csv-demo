class ParseCsvJob < ApplicationJob
  queue_as :parsing

  def perform(upload)
    upload.parse_csv!
  end
end
