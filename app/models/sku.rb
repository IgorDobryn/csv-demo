class Sku < ApplicationRecord
  belongs_to :supplier

  validates :field_1, presence: true
  validates :field_2, presence: true
  validates :field_3, presence: true
  validates :field_4, presence: true
  validates :field_5, presence: true
  validates :field_6, presence: true
  validates :price, presence: true, numericality: { greater_than: 0 }
end
