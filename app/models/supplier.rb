class Supplier < ApplicationRecord
  has_many :skus

  validates :name, presence: true, length: { maximum: 255 }
end
