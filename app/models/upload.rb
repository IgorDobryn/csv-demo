class Upload < ApplicationRecord
  include AASM

  CSV_FILENAME_REGEX = /(?<upload_type>supplier(s)?|sku(s)?)[\w\d\-\_\(\)]*\.csv\z/i

  has_attached_file :csv_file
  validates_attachment :csv_file, content_type: { content_type: ["text/comma-separated-values", "text/csv", "text/plain"] },
                       presence: true, matches: [CSV_FILENAME_REGEX]
  validates :rows_count, presence: true, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :processed_rows_count, presence: true, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :processed_rows_count, numericality: { less_than_or_equal_to: :rows_count }, if: :rows_count
  validates :upload_type, inclusion: [:supplier, :sku]

  aasm :status do
    state :pending, initial: true
    state :in_progress, :finished

    event :run do
      transitions from: :pending, to: :in_progress
    end

    event :finish do
      transitions from: :in_progress, to: :finished, guard: -> { processed_rows_count <= rows_count }
    end
  end

  def schedule_parse_csv_job
    ParseCsvJob.perform_later(self)
  end

  def parse_csv!
    UploadCsvParser.for(self).parse!
  end

  def upload_type
    return @upload_type if @upload_type

    if match = csv_file_file_name&.match(CSV_FILENAME_REGEX)
      @upload_type ||= match[:upload_type].downcase.singularize.to_sym
    end
  end
end
