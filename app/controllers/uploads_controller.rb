class UploadsController < ApplicationController
  def new
    @upload = Upload.new
  end

  def create
    @upload = Upload.new upload_params
    if @upload.save
      @upload.schedule_parse_csv_job
      redirect_to skus_path, flash: { success: 'Successfully created new upload' }
    else
      render :new
    end
  end

  def index
    @uploads = Upload.page(params[:page])
  end

  private

  def upload_params
    params.require(:upload).permit(:csv_file)
  end
end
