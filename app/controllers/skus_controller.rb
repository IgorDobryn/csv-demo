class SkusController < ApplicationController
  def index
    @skus = Sku.preload(:supplier).page(params[:page])
  end
end
