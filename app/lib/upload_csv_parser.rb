class UploadCsvParser
  COL_SEP = '¦'
  CHUNK_SIZE = 1000
  PARSERS = {
      supplier: 'SuppliersCsvParser',
      sku: 'SkusCsvParser'
  }
  PROCESSES = 16
  THREADS = 0

  def initialize upload
    @upload = upload
  end

  class << self
    def headers
      raise NotImplementedError
    end

    def value_converters
      raise NotImplementedError
    end

    def for upload
      PARSERS[upload.upload_type].constantize.new(upload)
    end
  end

  def parse!
    @upload.run!
    @upload.update rows_count: csv_chunks.sum(&:size)
    self.class.model.connection
    Parallel.map(csv_chunks, in_process: PROCESSES, in_threads: THREADS) do |csv_chunk|
      ActiveRecord::Base.connection_pool.with_connection do
        chunk = csv_chunk.reverse.uniq { |attrs| attrs[:id] }
        process_chunk chunk
        @upload.increment! :processed_rows_count, chunk.size
      end
    end

    @upload.finish!
  end

  private

  def csv_chunks
    @csv_chunks ||= SmarterCSV.process(@upload.csv_file.path,
                                       chunk_size: CHUNK_SIZE,
                                       col_sep: COL_SEP,
                                       headers_in_file: false,
                                       user_provided_headers: self.class.headers,
                                       value_converters: self.class.value_converters)
  end

  def process_chunk chunk
    existing_records =  self.class.model.transaction do
      self.class.model.where(id: chunk.map { |attrs| attrs[:id] }).group_by &:id
    end

    chunk.each do |attrs|
      record = existing_records[attrs[:id]]&.first || self.class.model.new(id: attrs[:id])

      record.assign_attributes attrs
      if record.changed?
        begin
          record.save!
        rescue
          @upload.increment! :invalid_rows_count
        end
      end
    end
  end
end
