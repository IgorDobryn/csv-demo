class SkusCsvParser < UploadCsvParser
  class << self
    def headers
      [:id, :supplier_id, :field_1, :field_2, :field_3, :field_4, :field_5, :field_6, :price]
    end

    def model
      Sku
    end

    def value_converters
      {
          supplier_id: SupplierIdConverter
      }
    end
  end
end
