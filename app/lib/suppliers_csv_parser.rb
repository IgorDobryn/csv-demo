class SuppliersCsvParser < UploadCsvParser
  class << self
    def headers
      [:id, :name]
    end

    def model
      Supplier
    end

    def value_converters
      {
          id: SupplierIdConverter
      }
    end
  end
end
