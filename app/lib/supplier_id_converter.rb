class SupplierIdConverter
  class << self
    def convert value
      return value if value.kind_of?(Integer)

      if match = value.match(/(?<id>\d+)/)
        match[:id].to_i
      end
    end
  end
end
