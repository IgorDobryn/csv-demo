```
Тестовое задание:

    Создать rails application с бд postgres, в котором будет реализовано:
    1. Upload CSV файла (товары и поставщики) и последующий фоновый парсинг этого CSV файла с сохранением результатов в бд, в зависимости от названия файла (описаны ниже).

    Для suppliers.csv:
    Cоздание либо обновление модели поставщика по ключу "Код поставщика"

    Структура файла:
    Код поставщика ¦ Название поставщика 

    Для sku.csv:
    Создание либо обновление модели товара по ключу "SKU". 
    Отдельного внимания требует поле "Код поставщика". Это поле сигнализирует о связи товара с полем "Код поставщика" в модели поставщика, созданной ранее. Требуется связать товар с поставщиком по коду.

    Структура файла:
    SKU ¦ Код поставщика ¦ Доп. Поле 1 ¦ Доп. Поле 2 ¦ Доп. Поле 3 ¦ Доп. Поле 4 ¦ Доп. Поле 5 ¦ Доп. Поле 6 ¦ Цена

    3. Отображение результатов парсинга товаров CSV в виде простой таблички
    4. Тесты

    Версия рельсов, руби, gem'ы, применяемые технологии - на свое усмотрение.
    Исходные данные для импорта – прилагаю.

```

1. Install ruby 
2. Bundle gems
3. Make sure you have yarn installed https://yarnpkg.com/lang/en/docs/install/
4. Install yarn packages ```yarn install```
5. Make sure you database and redis configs in config folder
6. Vreate db and populate test data 
- ```bundle exec rake db:create```
- ```bundle exec rake add_test_data```
7. Prepare test db ```bundle exec rake db:test:prepare```
8. Run tests ```bundle exec rake```
9. Run webpack server ```bin/webpack-dev-server```
10. Run sidekiq ```bundle exec sidekiq```
11. Run app server ```bundle exec rails s```
12. Visit application
