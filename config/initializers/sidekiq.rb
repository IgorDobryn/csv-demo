Sidekiq.configure_server do |config|
  config.redis = Rails.configuration.redis.symbolize_keys
end

Sidekiq.configure_client do |config|
  config.redis = Rails.configuration.redis.symbolize_keys
end
