Rails.application.routes.draw do
  root to: 'skus#index'
  resources :skus, only: [:index]
  resources :uploads, only: [:new, :create, :index]
end
